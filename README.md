# idesoft-java-spring
Desafío Idesoft

# Primeros pasos
- Desde la raiz del proyecto ejecutar:
mvn clean install

## Crear imagen Docker

docker build -t demo-idesoft .

## Iniciar Imagen Docker

docker run -p 8080:8080 demo-idesoft


## Swagger Holiday Service API 1.0 

http://localhost:8080/swagger-ui/#/

## Se adjunta colección para Postman

DEMO-IDESOFT.postman_collection.json




