package com.idesoft.demo.model;

import java.util.List;

public class HolidaysResponse {
	private String status;
	private List<Holiday> data;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Holiday> getData() {
		return data;
	}

	public void setData(List<Holiday> data) {
		this.data = data;
	}

}
