package com.idesoft.demo.services;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.idesoft.demo.model.Holiday;
import com.idesoft.demo.model.HolidaysResponse;

@Service
public class HolidayService {

	@Value("${feriados.service.url}")
	private String feriadosServiceUrl;

	public HolidaysResponse getAll() throws IOException {
		
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(feriadosServiceUrl);

		HttpResponse response = httpClient.execute(request);
		String json = EntityUtils.toString(response.getEntity(), "UTF-8");

		ObjectMapper objectMapper = new ObjectMapper();
		HolidaysResponse holidays = objectMapper.readValue(json, new TypeReference<HolidaysResponse>() {
		});

		return holidays;

	}

	public List<Holiday> getFilterByExtra(String extraFilter) throws IOException {

		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(feriadosServiceUrl);

		HttpResponse response = httpClient.execute(request);
		String json = EntityUtils.toString(response.getEntity(), "UTF-8");

		ObjectMapper objectMapper = new ObjectMapper();
		HolidaysResponse holidaysResponse = objectMapper.readValue(json, new TypeReference<HolidaysResponse>() {
		});

		List<Holiday> holidays = holidaysResponse.getData();

		List<Holiday> filteredHolidays = holidays.stream().filter(holiday -> holiday.getExtra().equals(extraFilter))
				.collect(Collectors.toList());

		return filteredHolidays;

	}

	public List<Holiday> getFilterByDates(String desde, String hasta) throws IOException {

		LocalDate startDate = LocalDate.parse(desde);
		LocalDate endDate = LocalDate.parse(hasta);

		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(feriadosServiceUrl);

		HttpResponse response = httpClient.execute(request);
		String json = EntityUtils.toString(response.getEntity(), "UTF-8");

		ObjectMapper objectMapper = new ObjectMapper();
		HolidaysResponse holidaysResponse = objectMapper.readValue(json, new TypeReference<HolidaysResponse>() {
		});

		List<Holiday> holidays = holidaysResponse.getData();

		List<Holiday> filteredHolidays = holidays.stream().filter(holiday -> {
			LocalDate holidayDate = LocalDate.parse(holiday.getDate());
			return holidayDate.isAfter(startDate) && holidayDate.isBefore(endDate);
		}).collect(Collectors.toList());

		return filteredHolidays;

	}
}
