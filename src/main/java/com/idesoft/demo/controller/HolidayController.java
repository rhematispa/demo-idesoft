package com.idesoft.demo.controller;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.idesoft.demo.model.Holiday;
import com.idesoft.demo.model.HolidaysResponse;
import com.idesoft.demo.services.HolidayService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;



@RestController
@RequestMapping("/holidays")
@Api(value = "Holiday Management System", tags = "Holidays")
public class HolidayController {
  @Autowired
  private HolidayService holidayService;
  
  private static final Logger logger = LogManager.getLogger(HolidayController.class);  
  
  @GetMapping("/")
  @ApiOperation(value = "Obtener todos los días festivos", response = HolidaysResponse.class)
  public HolidaysResponse getAll() throws IOException {
	  logger.info("Obtener todos los días festivos");
	  return holidayService.getAll();
  }
  
  
  @GetMapping("/filterByExtra/{extra}")
  @ApiOperation(value = "Filtrar festivos por propiedad extra", response = List.class)
  public List<Holiday> getFilterByExtra(@PathVariable String extra) throws IOException {
	  logger.info("Filtrar festivos por propiedad extra");
	  return holidayService.getFilterByExtra(extra);
  }
  
  
  @GetMapping("/filterByDates/{desde}/{hasta}")
  @ApiOperation(value = "Filtrar festivos por rango de fechas: date", response = List.class)
  public List<Holiday> getFilterByDates(@PathVariable String desde, @PathVariable String hasta) throws IOException {
	  logger.info("Filtrar festivos por rango de fechas: date");
	  return holidayService.getFilterByDates(desde, hasta);
  }
  
}

